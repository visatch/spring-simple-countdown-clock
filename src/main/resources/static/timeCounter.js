let countDown = function(theSelector, time){
    let output = "";
    let dTime = Date.parse(time);
    let theDate = Date.parse(new Date());
    let difference = dTime - theDate;
    let milliseconds = difference % 1000
    let seconds

    function addZero(number){
        if(number <= 9){
            number = "0" + number;
        }
        return number;
    }

    let x = difference / 1000;
    seconds = addZero(parseInt(x % 60));
    x /= 60;
    let minutes = addZero(parseInt(x % 60));
    x /= 60;
    let hours = addZero(parseInt(x % 24));
    x /= 24;
    let days = addZero(parseInt(x));

    output += "<div class='days w-5'>" + days + "<small>Days</small></div>";
    output += "<div class='hours w-5'>" + hours + "<small>Hours</small></div>";
    output += "<div class='minutes w-5'>" + minutes + "<small>Minutes</small></div>";
    output += "<div class='seconds w-5'>" + seconds + "<small>Seconds</small></div>";
    document.querySelector(theSelector).innerHTML = output;
}